cc=clang
cf=-march=native -mtune=native -O2
af=-masm=intel -S
df=-g
./tablecalc:	./tablecalc.c
	$(cc) $(cf) ./tablecalc.c -o ./tablecalc
asm:		./tablecalc.c
	$(cc) $(cf) $(af) ./tablecalc.c -o ./tablecalc
debug:		./tablecalc.c
	$(cc) $(cf) $(gf) ./tablecalc.c -o ./tablecalc
install:	./tablecalc
	sudo cp ./tablecalc /usr/bin/
uninstall:
	sudo rm /usr/bin/tablecalc 
