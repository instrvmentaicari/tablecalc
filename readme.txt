Tablecalc will do various calculations on 2 dimensions of textual data from stdin.
The default calculation is averaging.
For example, take this 2 dimensional table:

4 5 6 7 8
1 2 3 4 5
8 9 1 2 3
7 6 4 5 9

When fed through tablecalc, will give the average of each column, which is:

5 5.5 3.5 4.5 6.25 

It does this by counting whitespace characters and lines.
Whitespace (spaces and tabs) count as x-axis increments while newlines count as y-axis decrements.
From this fact of operation, double whitespace is treated as a zero value.
The following tables have equivalent averages.

4 5 6 7 8
1 2 3 4 5
8  1 2 3
7 6 4 5 9

4 5 6 7 8
1 2 3 4 5
8 0 1 2 3
7 6 4 5 9

5 3.5 3.5 4.5 6.25 

Single column averages also work, which is useful for quickly averaging a linebroken list of
grep'd and parsed statistics.

4
1
8
7

5

Also does greatest and least values in each column (see usage with "tablecalc h").
Does calculations in static, 2d array of long doubles (8 byte floats) with dimensions of 32 columns
 and 128 rows.
