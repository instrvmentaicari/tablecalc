#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#define debug 0
#define maxheight 128
#define maxwidth 32
char *help= "tablecalc usage: tablecalc [mode]\n\n"
            " Description: Performs calculations on a column of a table of data organized by spaces/tabs.\n"
            "  and newlines.\n\n" 
            " Modes: (case-insensitive)\n"
            "\ta - Average. (default)\n"
            "\tt - Top(greatest) values in each column.\n"
            "\tb - Bottom(least) values in each column.\n"
            "\tm - Median. (not yet implemented)\n"
            "\th - Display this usage message.\n";

int gline (int *r, long double *l);
int maxcount (int *c, int n);
int prntavgs (long double l[maxheight][maxwidth], int w, int h);
int prntmeds (long double l[maxheight][maxwidth], int w, int h);
int prnttops (long double l[maxheight][maxwidth], int w, int h);
int prntbots (long double l[maxheight][maxwidth], int w, int h);
int main (int argc, char** argv){
 long double tbls[maxheight][maxwidth];
 int counts[maxheight];
 int r= 1, i=0; 
 for (; i<maxheight&&r; i++)
  counts[i]= gline (&r, tbls[i]); 
 i--;
 if (argc>1){
  switch (argv[1][0]){
   case 'H':
   case 'h':
   default:
    write (2, help, strlen (help)); return 1;
   case 'A':
   case 'a': prntavgs (tbls, maxcount (counts, i), i); break;
   case 'T':
   case 't': prnttops (tbls, maxcount (counts, i), i); break;
   case 'B':
   case 'b': prntbots (tbls, maxcount (counts, i), i); break;
   case 'M':
   case 'm': prntmeds (tbls, maxcount (counts, i), i); break;
  }
 } else prntavgs (tbls, maxcount (counts, i), i);
 return 0;
}
int maxcount (int *c, int n){
 int r= 0;
 for (int i= 0; i<n; i++)
  r= (c[i]>r?c[i]:r);
 #if debug
 printf ("maxcount %i\n",r);
 #endif
 return r;
}

int gline (int *r, long double *l){
 char ireg[maxwidth+1]= {0};
 int i= 0, n= 0; *r=1;
 long double t;
 while (n<maxwidth&&i<maxwidth&&*r){
  *r= read (0, ireg+i, 1);
  if (t= ireg[i], t=='\t'||t==' '||t=='\n'||t==0){
   ireg[i]= 0, l[n]=atof (ireg);
   #if debug
   printf ("gline iter %i %s\n", n, ireg);
   #endif
   i= 0, n++;
   if (t=='\t'||t==' ') continue;
   else return n;
  } else i++;
 }
 return n;
}
int prntavgs (long double l[maxheight][maxwidth], int w, int h){
 #if debug
 printf ("prntavgs started\n");
 #endif
 for (int x= 0; x<w; x++){
  #if debug
  printf ("xloop iter\n");
  #endif
  long double line[h],o= 0.0;
  for (int y= 0; y<h; y++){
   o+= l[y][x]/(long double)h;
    #if debug
    printf ("iloop, yloop res %Lf\n",o);
    #endif
  }
  printf ("%Lf ",o);
 }
 printf ("\n");
 #if debug
 printf ("prntavgs ended\n");
 #endif
 return 0;
}
// If anyone has an idea of how to find a median without a sort let me know.
int prntmeds (long double l[maxheight][maxwidth], int w, int h){
 for (int x= 0; x<w; x++){
  long double line[h], high= l[0][x], low= l[0][x], t;
  for (int y= 0; y<h; y++)
   t= l[y][x], line[y]= t, high= (high<t?t:high), low= (low>t?t:low);
  long double range= high-low, mid= low +(range/2.0); int offset= 0;
  for (int y= 0;  y<h; y++) t= line[y], offset= offset+(t>mid? 1: -1);
  printf ("%i\n", offset);
 }
 printf ("\n");
 return 0;
}
int prnttops (long double l[maxheight][maxwidth], int w, int h){
 for (int x= 0; x<w; x++){
  long double t, n= l[0][x];
  for (int y= 0; y<h; y++)
   t= l[y][x], n= t>n? t: n;
  printf ("%Lf ", n);
 }
 printf ("\n");
 return 0;
}
int prntbots (long double l[maxheight][maxwidth], int w, int h){
 for (int x= 0; x<w; x++){
  long double t, n= l[0][x];
  for (int y= 0; y<h; y++){
   t= l[y][x], n= t<n? t: n;
   #if debug
   printf ("prntlows iter %Lf\n", n);
   #endif
   }
  printf ("%Lf ", n);
 }
 printf ("\n");
 return 0;
}
